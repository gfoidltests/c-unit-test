﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "Fortran", Scope = "member", Target = "~M:UnitTest.Bsp.Fortran.Tests.MatheTests.Addiere.addiere(System.Int32@,System.Int32@)~System.Int32")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "Fortran", Scope = "member", Target = "~M:UnitTest.Bsp.Fortran.Tests.MatheTests.Subtrahiere.subtrahiere(System.Int32@,System.Int32@)~System.Int32")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "Fortran", Scope = "member", Target = "~M:UnitTest.Bsp.Fortran.Tests.MatheTests.Dividiere.dividiere(System.Int32@,System.Int32@)~System.Int32")]

