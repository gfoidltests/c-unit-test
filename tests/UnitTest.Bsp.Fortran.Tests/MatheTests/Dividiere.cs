﻿using System;
using System.Runtime.InteropServices;
using NUnit.Framework;

namespace UnitTest.Bsp.Fortran.Tests.MatheTests
{
	[TestFixture]
	public class Dividiere
	{
		[Test]
		public void Args_given___correct_sum()
		{
			int a = 12;
			int b = 4;

			int actual = dividiere(ref a, ref b);

			Assert.AreEqual(3, actual);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Divisor_is_0___throws_DivByZero()
		{
			int a = 12;
			int b = 0;

			Assert.Throws<DivideByZeroException>(() =>
			{
				int actual = dividiere(ref a, ref b);
			});
		}
		//---------------------------------------------------------------------
		[DllImport("UnitTest.Bsp.Fortran.dll")]
		private static extern int dividiere(ref int a, ref int b);
	}
}