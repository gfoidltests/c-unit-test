﻿using System.Runtime.InteropServices;
using NUnit.Framework;

namespace UnitTest.Bsp.Fortran.Tests.MatheTests
{
	[TestFixture]
	public class Addiere
	{
		[Test]
		public void Args_given___correct_sum()
		{
			int a = 3;
			int b = 4;

			int actual = addiere(ref a, ref b);

			Assert.AreEqual(7, actual);
		}
		//---------------------------------------------------------------------
		[DllImport("UnitTest.Bsp.Fortran.dll")]
		private static extern int addiere(ref int a, ref int b);
	}
}