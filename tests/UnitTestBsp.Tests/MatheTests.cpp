#include <stdexcept>
#include "stdafx.h"
#include "CppUnitTest.h"
#include "..\..\source\UnitTestBsp\Mathe.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTestBspTests
{
	TEST_CLASS(MatheTests)
	{
	public:
		TEST_METHOD(Addiere_ArgsGiven_CorrectSum)
		{
			Mathe mathe;

			int actual = mathe.Addiere(3, 4);

			Assert::AreEqual(7, actual);
		}
		//---------------------------------------------------------------------
		TEST_METHOD(Dividiere_DivisorIs0_ThrowsDivideByZero)
		{
			Mathe mathe;
			int a = 7;
			int b = 0;

			try
			{
				int actual = mathe.Dividiere(a, b);
			}
			catch (std::invalid_argument ex)
			{
				return;		// Correct exception
			}
			catch (...)
			{
				wchar_t message[200];
				_swprintf(message, L"Incorrect exception for %g", b);
				Assert::Fail(message, LINE_INFO());
			}

			throw std::exception("Should not be here");
		}
	};
}