integer function addiere(a, b)
	implicit none
	integer, intent(in)			:: a, b
	
	addiere = a + b
end function addiere

integer function subtrahiere(a, b)
	implicit none
	integer, intent(in)			:: a, b
	
	subtrahiere = a - b
end function subtrahiere

integer function dividiere(a, b)
	implicit none
	integer, intent(in)			:: a, b
	
	dividiere = a / b
end function dividiere