#include <stdexcept>
#include "Mathe.h"
//-----------------------------------------------------------------------------
int Mathe::Addiere(const int a, const int b)
{
	return a + b;
}
//-----------------------------------------------------------------------------
int Mathe::Subtrahiere(const int a, const int b)
{
	return a - b;
}
//-----------------------------------------------------------------------------
int Mathe::Dividiere(const int a, const int b)
{
	if (b == 0) throw std::invalid_argument("b must not be 0");

	return a / b;
}